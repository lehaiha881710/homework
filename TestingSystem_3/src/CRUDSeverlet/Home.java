package CRUDSeverlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class haiha1710
 */
@WebServlet("/haiha1710")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = getCookieByUserName(request, "userName_key");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("userName="+userName);
		if("admin".equals(userName)) {
			response.sendRedirect("http://localhost:8080/CRUDTestingSystem_3/Admin/index.jsp");
		}else
		{
			response.sendRedirect("http://localhost:8080/CRUDTestingSystem_3/User/index.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private String getCookieByUserName(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
		if(cookies==null) {
			return null;
		}
		for(Cookie cookie:cookies) {
			if(cookie.getName().equals(key)) {
				return cookie.getValue();
			}
		}
		return null;
	}

}
