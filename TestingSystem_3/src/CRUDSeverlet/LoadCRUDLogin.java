package CRUDSeverlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoadCRUDLogin
 */
@WebServlet("/LoadCRUDLogin")
public class LoadCRUDLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadCRUDLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String tk = request.getParameter("tk");
		String mk = request.getParameter("mk");

		
		if ("admin".equals(tk) && "123456".equals(mk)) {
			
			Cookie cookie = new Cookie("userName_key", tk);
			response.addCookie(cookie);
			
			response.sendRedirect("http://localhost:8080/CRUDTestingSystem_3/haiha1710");
		}
		else if("user".equals(tk)&&"123456".equals(mk)) {
			Cookie cookie = new Cookie("userName_key", tk);
			response.addCookie(cookie);
			response.sendRedirect("http://localhost:8080/CRUDTestingSystem_3/haiha1710");
		}
		else {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print("Error lòi cả mắt kìa má");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/Login.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
