<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>CRUD Demo</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/program.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/navi.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/table.css">
</head>

<body>
	<div class="header"></div>

	<div class="main"></div>

	<div class="footer"></div>
	<script>
		$(function() {
				$(".header").load("${pageContext.request.contextPath}/header.html");
				$(".main").load("${pageContext.request.contextPath}/home.html");
				$(".footer").load("${pageContext.request.contextPath}/footer.html");

		});
		function fillEmployeeTotable(employees) {
			employees
					.forEach(function(employee) {
						$('tbody')
								.append(
										'<tr>'
												+ '<td style="display:none;">'
												+ employee.id
												+ '</td>'
												+ '<td>'
												+ employee.name
												+ '</td>'
												+ '<td>'
												+ employee.department
												+ '</td>'
												+ '<td>'
												+ employee.phone
												+ '</td>'
												+ '<td>'
												+ '<a class="edit" title="Edit" onclick="updateEmployee('
												+ employee.id
												+ ')" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>'
												+ '<a class="delete" title="Delete" onclick="deleteEmployee('
												+ employee.id
												+ ')" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>'
												+ '</td>' + '</tr>')
					});
		}

		function initEmployees() {
			$.ajax({
				url : "https://5f773431d5c9cb0016236e7c.mockapi.io/employees",
				type : 'GET',
				success : function(result) {
					fillEmployeeTotable(result);
				},
				error : function(error) {
					alert("Error");
				}
			})
		}

		function addEmployeess() {
			// $.get("https://5f773431d5c9cb0016236e7c.mockapi.io/employees", function(data, status) {
			//     fillEmployeeTotable(data);
			// });
			$('#exampleModal').modal('show');
			var name = document.getElementById("username").value;
			var department = document.getElementById("department").value;
			var phone = document.getElementById("phone").value;
			if (name == "") {
				alert("Tên không được trống!!");
			} else if (department == "") {
				alert("Department không được trống!!");
			} else if (phone == "") {
				alert("Phone không được trống!!");
			} else {
				var newEmployee = {
					"name" : name,
					"department" : department,
					"phone" : phone
				};
				$
						.ajax({
							url : "https://5f773431d5c9cb0016236e7c.mockapi.io/employees",
							data : newEmployee,
							type : 'POST',
							success : function(result) {
								console.log("result add:" + result);
								alert("Add success!!");
								initEmployees();
								$('#exampleModal').modal('hide');
								$(".main").load("${pageContext.request.contextPath}/listemployees.html");
							}
						});
			}
		}
		function updateEmployee(idClickUpdate) {
			$
					.ajax({
						url : "https://5f773431d5c9cb0016236e7c.mockapi.io/employees/"
								+ idClickUpdate,
						type : 'PUT',
						success : function(result) {
							if (result == undefined || result == null) {
								alert("Error when loading data");
							} else {
								document.getElementById("username1").value = result.name;
								document.getElementById("department1").value = result.department;
								document.getElementById("phone1").value = result.phone;
								$('#exampleModal1').modal('show');
							}
						},
						error : function(error) {
							alert("Error");
						}
					});
			$('#btnSaveChange')
					.click(
							function() {
								var name = document.getElementById("username1").value;
								var department = document
										.getElementById("department1").value;
								var phone = document.getElementById("phone1").value;
								var newEmployee = {
									"name" : name,
									"department" : department,
									"phone" : phone
								}
								$
										.ajax({
											url : "https://5f773431d5c9cb0016236e7c.mockapi.io/employees/"
													+ idClickUpdate,
											data : newEmployee,
											type : 'PUT',
											success : function(result) {
												alert("Update success!!");
												initEmployees();
												$('#exampleModal').modal('hide');
												$(".main").load("${pageContext.request.contextPath}/listemployees.html");
											},
											error : function(error) {
												alert("Error");
											}
										});
							});
		}
		function deleteEmployee(idClickDelete) {
			var lc = confirm("Bạn có muốn xóa không!");
			if (lc == true) {
				$
						.ajax({
							url : "https://5f773431d5c9cb0016236e7c.mockapi.io/employees/"
									+ idClickDelete,
							type : 'DELETE',
							success : function(result) {
								console.log("result delete:" + result);
								alert("Delete success!!");
								$(".main").load("${pageContext.request.contextPath}/listemployees.html");
								initEmployees();
							}
						})
			} else
				alert("Đã hủy!!");
		}
		function Employee(name, department, phone) {
			this.name = name;
			this.department = department;
			this.phone = phone;
		}

		function clickHomeButton() {
			$(".main").load("${pageContext.request.contextPath}/home.html");
		}

		function clickViewListEmployeeButton() {
			$(".main").load("${pageContext.request.contextPath}/listemployees.html");
			initEmployees();
		}
	</script>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form>
					<div class="modal-body">
						<div class="form-group">
							<label for="username">Username:</label> <input type="text"
								class="form-control" id="username" placeholder="Enter username"
								name="username" required>

						</div>
						<div class="form-group">
							<label for="department">Derpartment:</label> <input type="text"
								class="form-control" id="department"
								placeholder="Enter Department" name="depamertment" required>
						</div>
						<div class="form-group">
							<label for="phone">Phone:</label> <input type="text"
								class="form-control" id="phone" placeholder="Enter Phone"
								name="depamertment" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Cancer</button>
						<button type="button" class="btn btn-primary"
							onclick="addEmployeess()">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel1">Update
						Employee</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form>
					<div class="modal-body">
						<div class="form-group">
							<label for="username">Username:</label> <input type="text"
								class="form-control" id="username1" placeholder="Enter username"
								name="username" required>

						</div>
						<div class="form-group">
							<label for="department">Derpartment:</label> <input type="text"
								class="form-control" id="department1"
								placeholder="Enter Department" name="depamertment" required>
						</div>
						<div class="form-group">
							<label for="phone">Phone:</label> <input type="text"
								class="form-control" id="phone1" placeholder="Enter Phone"
								name="depamertment" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Cancer</button>
						<button type="submit" class="btn btn-primary" id="btnSaveChange">Save
							changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>

</html>