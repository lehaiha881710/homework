package com.vti.test4.service;

import java.util.List;

import com.vti.test4.entity.Department;
import com.vti.test4.repository.DepartmentRepository;

public class DepartmentServiceImpl implements DepartmentService{
	public DepartmentRepository departmentRepository;
	public DepartmentServiceImpl() {
		departmentRepository = new DepartmentRepository();
	}
	public void createDepartment() {
		departmentRepository.createDepartment();
	}
	public List<Department> getDepartment() {
		return departmentRepository.getAllDepartment();
	}
	public Department getDepartmentByName(String name) {
		return  departmentRepository.getDepartmentByName(name);
	}
	public Department getDepartmentByID(short id) {
		return departmentRepository.getDepartmentById(id);
	}
	public void updateDepartment(short id, String newName) {
		departmentRepository.updateDepartment(id, newName);
	}
	public void deleteDepartment(short id) {
		departmentRepository.deleteDepartment(id);
	}
	public Boolean isGroupExistsByID(short id) {
		return departmentRepository.isGroupExistsByID(id);
	}
	public Boolean isGroupExistsByName(String name) {
		return departmentRepository.isGroupExistsByName(name);
	}
}
