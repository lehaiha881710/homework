package com.vti.test4.service;

import java.util.List;

import com.vti.test4.entity.Department;

public interface DepartmentService {
	void createDepartment();
	List<Department> getDepartment();
	Department getDepartmentByName(String name);
	Department getDepartmentByID(short id);
	void updateDepartment(short id, String newName);
	void deleteDepartment(short id);
	Boolean isGroupExistsByID(short id);
	Boolean isGroupExistsByName(String name);
}
