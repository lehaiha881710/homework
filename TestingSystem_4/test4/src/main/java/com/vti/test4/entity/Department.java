package com.vti.test4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="Department", catalog="TestingSystem1")
@Inheritance(strategy = InheritanceType.JOINED)
public class Department {
	private static final long  serialVersionID =1L;
	@Id
	@Column(name="DepartmentID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private short id;
	
	@Column(name="DepartmentName", length = 30, unique = true, nullable = true)
	private String name;
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}
	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
