package com.vti.test4.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import com.vti.test4.entity.Department;

public class DepartmentRepository {
	private SessionFactory sessionFactory;

	public void createDepartment() {
		// create Session fatory
		Session session = buildSessionFactory().openSession();
		// create session

		session.beginTransaction();
		// xử lí handing
		Department department = new Department();
		department.setName("Rocket-06");
		session.save(department);
		System.out.println("Create Success");
		session.getTransaction().commit();
	}

	public List<Department> getAllDepartment() {
		List<Department> departments = null;
		Session session = null;
		try {
			session = buildSessionFactory().openSession();
			session.beginTransaction();
			Query<Department> query = session.createQuery("FROM Department");
			departments = query.list();
			return departments;
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session != null)
				session.close();
		}
		return departments;
	}

	public Department getDepartmentByName(String name) {
		Department department = new Department();
		Session session = null;
		try {
			session = buildSessionFactory().openSession();
			session.beginTransaction();
			Query<Department> query = session.createQuery("FROM Department WHERE name =: nameParameter");
			query.setParameter("nameParameter", name);
			department = query.uniqueResult();
			return department;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public Department getDepartmentById(short id) {
		Department department = new Department();
		Session session = null;
		try {
			session = buildSessionFactory().openSession();
			session.beginTransaction();

			department = session.get(Department.class, id);

			return department;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public void updateDepartment(short id, String newName) {
		Session session = null;
		try {

			// get session
			session = buildSessionFactory().openSession();
			session.beginTransaction();

			// get department
			Department department = (Department) session.load(Department.class, id);

			// update
			department.setName(newName);

			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	public void deleteDepartment(short id) {
		Session session = null;
		try {
			// get session
			session = buildSessionFactory().openSession();
			session.beginTransaction();
			// get department
			Department department = (Department) session.load(Department.class, id);
			// delete
			session.delete(department);
			session.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	public Boolean isGroupExistsByID(short id) {
		if(getDepartmentById(id)!=null)
			return true;
		return false;
	}
	public Boolean isGroupExistsByName(String name) {
		if(getDepartmentByName(name)!=null)
			return true;
		return false;
	}
	private static SessionFactory buildSessionFactory() {
		// load configuration
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");

		// add entity
		configuration.addAnnotatedClass(Department.class);

		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();

		return configuration.buildSessionFactory(serviceRegistry);
	}
}
