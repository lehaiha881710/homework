package com.vti.test4.ui;

import java.util.List;

import com.vti.test4.controller.DepartmentController;
import com.vti.test4.entity.Department;

public class program {

	public static void main(String[] args) {
		DepartmentController controller = new DepartmentController();
		/**Create**/
				 controller.createDepartment();
		/** GetALLDepartment **/
				System.out.println(controller.getDepartment());
		/** GetDepartmentByName **/
				System.out.println(controller.getDepartmentByName("Rocket-06"));
		/** GetDepartmentById **/
				System.out.println(controller.getDepartmentById((short) 3));
		/** UpdateDepartment **/
				controller.updateDepartment((short) 3, "Bảo vệ Update");
		/** DeleteDepartment **/
				controller.deleteDepartment((short) 3);
		/**isGroupExistsByID**/
				if(controller.isGroupExistsByID((short) 3)==true) {
					System.out.println("Có tồn tại id Department này");
				}else
					System.out.println("Không tồn tại id Department này");
		/**isGroupExistsByName**/
				if(controller.isGroupExistsByName("Rocket-06")==true) {
					System.out.println("Có tồn tại name Department này");
				}else
					System.out.println("Không tồn tại name Department này");
	}
}
