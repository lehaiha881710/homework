package com.vti.test4.controller;

import java.util.List;

import com.vti.test4.entity.Department;
import com.vti.test4.service.DepartmentService;
import com.vti.test4.service.DepartmentServiceImpl;
public class DepartmentController {
	public DepartmentService departmentService;
	
	public DepartmentController(){
		departmentService =  new DepartmentServiceImpl();
	}
	public void createDepartment() {
		departmentService.createDepartment();
	}
	public List<Department> getDepartment() {
		return departmentService.getDepartment();
	}
	public Department getDepartmentByName(String name) {
		return departmentService.getDepartmentByName(name);
	}
	public Department getDepartmentById(short id) {
		return departmentService.getDepartmentByID(id);
	}
	public void updateDepartment(short id, String newName) {
		departmentService.updateDepartment(id, newName);
	}
	public void deleteDepartment(short id) {
		departmentService.deleteDepartment(id);
	}
	public Boolean isGroupExistsByID(short id) {
		return departmentService.isGroupExistsByID(id);
	}
	public Boolean isGroupExistsByName(String name) {
		return departmentService.isGroupExistsByName(name);
	}
}
