<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
    table,
    th,
    td {
      border: 1px solid black
    }

    form {
      margin: 0 auto;
      width: fit-content;
      margin-top: 20px;
      padding: 20px;
      border: 1px solid black
    }
  </style>
</head>
<body>
  <div style="width: 50vw; margin: 0 auto">
    <table style="width: 100%;">
      <tr>
        <th>STT</th>
        <th>Tên VXL</th>
        <th>Hãng sx</th>
        <th>Ngày ra mắt</th>
        <th>Giá (vnđ)</th>
      </tr>
      
    </table>

    <div style="float: right;">
      <form action="">
        <div>
          <label for="name">Tên VXL:</label>
          <br>
          <input type="text" id="name" name="name">
        </div>
        <div>
          <label for="brand">Hãng:</label>
          <br>
          <input type="text" id="brand" name="brand">
        </div>
        <div>
          <label for="date">Ngày ra mắt:</label>
          <br>
          <input type="date" id="date" name="date">
        </div>
        <div>
          <label for="price">Giá:</label>
          <br>
          <input type="text" id="price" name="price">
        </div>

        <div style="margin-top: 10px">
          <button type="submit">Thêm</button>
          <button type="button">Hủy</button>
        </div>
      </form>
    </div>
  </div>
</body>